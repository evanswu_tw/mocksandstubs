package cashregister;

// any of the mocks and stubs should be in the test folder instead as they
// are use for testing
// and its good to name your class based on whether its mock or stub
// for example mockTaxi or StubRecipes
class stubPrinter extends Printer {
    public boolean hasBeenCalled = false;
    public String printString = "";

    @Override
    public void print(String printThis) {
        this.hasBeenCalled = true;
        this.printString = printThis;
    }
}
