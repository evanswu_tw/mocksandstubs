package cashregister;

// any of the mocks and stubs should be in the test folder instead as they
// are use for testing
// and its good to name your class based on whether its mock or stub
// for example mockTaxi or StubRecipes
class stubPurchase extends Purchase {


    public stubPurchase(Item[] items) {
        super(items);
    }

    @Override
    public String asString() {
        return "Test";
    }
}
