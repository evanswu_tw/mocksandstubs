package cashregister;

import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class CashRegisterTest {
    private Printer mockPrinter;

    @Test
    public void shouldPrintPurchaseWithoutMockito() {
        stubPrinter stubPrinter = new stubPrinter();
        Item item = new Item("Macbook Pro", 2000.0);
        Item[] items = new Item[]{item};

        CashRegister cashRegister = new CashRegister(stubPrinter);
        cashRegister.process(new Purchase(items));
        assertEquals(true, stubPrinter.hasBeenCalled);
    }

    // when using Mockito, we would like to mock both printer and purchase 
    // the focus on this test is to ensure the cashRegister is working as expected
    // i.e. in this case, printing the string that we want. 
    // you might want to have a look at this - https://gojko.net/2009/10/23/mockito-in-six-easy-examples/
    // and read more about the usage of when thenReturn
    @Test
    public void shouldPrintPurchaseWithMockito() {
        mockPrinter = mock(Printer.class);

        String expectResult = "Macbook Pro\t2000.0\n";

        Purchase mockPurchase = mock(Purchase.class);
        when(mockPurchase.asString()).thenReturn(expectResult);

        CashRegister cashRegister = new CashRegister(mockPrinter);

        cashRegister.process(mockPurchase);
        verify(mockPrinter).print(expectResult);
    }

    // see previous comment
    @Test
    public void shouldPrintNullPurchaseWithMockito() {
        String expectResult = "No Purchase!";
        mockPrinter = mock(Printer.class);

        Purchase fakePurchase = mock(Purchase.class);
        when(fakePurchase.asString()).thenReturn(expectResult);

        CashRegister cashRegister = new CashRegister(mockPrinter);
        cashRegister.process(null);

        verify(mockPrinter).print(expectResult);

    }

}
